package pixiv

import (
	"encoding/json"
	"errors"
	"fmt"
	"math/rand"
	"net/http"
	"reflect"
	"strings"
	"time"
)

type response struct {
	Contents  []Illustration `json:"contents"`
	Mode      string         `json:"mode"`
	Content   string         `json:"content"`
	Page      int            `json:"page"`
	Prev      bool           `json:"prev"`
	Next      int            `json:"next"`
	Date      string         `json:"date"`
	PrevDate  string         `json:"prev_date"`
	NextDate  bool           `json:"next_date"`
	RankTotal int            `json:"rank_total"`
}

type response2 struct {
	Status   string         `json:"status"`
	Response []Illustration `json:"response"`
}

var header = map[string][]string{
	"Accept":          {"*/*"},
	"Accept-Language": {"zh-CN,zh;q=0.8,gl;q=0.6,zh-TW;q=0.4"},
	"Host":            {"www.pixiv.net"},
	"Referer":         {"https://www.pixiv.net/"},
	"User-Agent":      {"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36"},
	"Cookie":          {"PHPSESSID=7473695_d64d55ef3f3883e34657a8e888b24a56"},
}

func init() {
	rand.Seed(time.Now().Unix())
}

func RandDailyR18Illust(filter *Filter) (*Illustration, error) {
	tries := 0

beginning:
	if tries >= 5 {
		return nil, errors.New("cannot find a suitable artwork after 5 tries")
	}

	tries += 1
	p := 1
	i := rand.Intn(100)
	if i >= 50 {
		p = 2
		i -= 50
	}

	url := fmt.Sprintf("https://www.pixiv.net/ranking.php?mode=daily_r18&content=illust&format=json&p=%d", p)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	req.Header = header

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()

	resp := response{}
	dec := json.NewDecoder(res.Body)
	dec.Decode(&resp)

	illust := &resp.Contents[i]
	illust.Href = fmt.Sprintf("https://www.pixiv.net/member_illust.php?mode=medium&illust_id=%d", illust.IllustID)

	// check filter by type
	fields := reflect.ValueOf(&filter.Type).Elem()
	for i := 0; i < fields.NumField(); i++ {
		switch fields.Field(i).Type().Name() {
		case "bool":
			if fields.Field(i).Bool() {
				fieldName := fields.Type().Field(i).Name
				if reflect.ValueOf(&illust.IllustContentType).Elem().FieldByName(fieldName).Bool() {
					goto beginning
				}
			}
			// TODO: support other data type
		}
	}

	illust.ImageUrls.Small = illust.Url

	err = setOriginalImage(illust)
	if err != nil {
		return nil, err
	}

	return illust, nil
}

func ById(id, page int) (*Illustration, error) {
	url := fmt.Sprintf("https://api.imjad.cn/pixiv/v1/?type=illust&id=%d", id)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	req.Header = header

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()

	resp := response2{}
	dec := json.NewDecoder(res.Body)
	dec.Decode(&resp)

	if len(resp.Response) < 0 {
		return nil, errors.New(fmt.Sprintf("illustration of id %d not found", id))
	}

	illust := &resp.Response[0]
	if page != 0 {
		illust.ImageUrls = illust.Metadata.Pages[page].ImageUrls
	}
	illust.Url = illust.ImageUrls.Large

	err = setOriginalImage(illust)
	if err != nil {
		return nil, err
	}

	return illust, nil
}

func setOriginalImage(illust *Illustration) error {
	// resampled: https://i.pximg.net/c/240x480/img-master/img/2018/11/20/10/00/01/71741057_p0_master1200.jpg
	// original:  https://i.pximg.net          /img-master/img/2018/11/20/10/00/01/71741057_p0_master1200.jpg
	// PNG:       https://i.pximg.net          /img-master/img/2018/11/20/10/00/01/71741057_p0_master1200.png

	// tryJpg:
	url := strings.Replace(illust.Url, "/c/240x480", "", 1)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return err
	}

	req.Header = header

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		goto tryPng
	}

	if res.StatusCode == 404 {
		res.Body.Close()
		goto tryPng
	}

	illust.ImageUrls.Large = url
	illust.ImageContentType = "image/jpeg"
	illust.ImageReader = res.Body // potential mem leak
	return nil

tryPng:
	url = strings.Replace(url, "jpg", "png", 1)

	req, err = http.NewRequest("GET", url, nil)
	if err != nil {
		return err
	}

	req.Header = header

	res, err = http.DefaultClient.Do(req)
	if err != nil {
		return err
	}

	illust.ImageUrls.Large = url
	illust.ImageContentType = "image/png"
	illust.ImageReader = res.Body // potential mem leak
	return nil
}
