package util

import (
	"errors"
	"fmt"
	"strings"
	"unicode"
	"unicode/utf8"
)

const (
	noTokenState = iota
	normalTokenState
	signleQuoteState
	doubleQuoteState
)

func Tokenize(arguments string) ([]string, error) {
	var argList []string

	var currArg strings.Builder
	escaped := false
	state := noTokenState // start in the NO_TOKEN_STATE
	length := utf8.RuneCountInString(arguments)

	for i := 0; i < length; i++ {
		c := []rune(arguments)[i]
		if escaped {
			// Escaped state: just append the next character to the current arg.
			escaped = false
			currArg.WriteRune(c)
		} else {
			switch state {
			case signleQuoteState:
				if c == '\'' {
					// Seen the close quote; continue this arg until whitespace is seen
					state = normalTokenState
				} else {
					currArg.WriteRune(c)
				}
			case doubleQuoteState:
				if c == '"' {
					// Seen the close quote; continue this arg until whitespace is seen
					state = normalTokenState
				} else if c == '\\' {
					// Look ahead, and only escape quotes or backslashes
					i++
					next := []rune(arguments)[i]
					if next == '"' || next == '\\' {
						currArg.WriteRune(next)
					} else {
						currArg.WriteRune(c)
						currArg.WriteRune(next)
					}
				} else {
					currArg.WriteRune(c)
				}
			case noTokenState, normalTokenState:
				switch c {
				case '\\':
					escaped = true
					state = normalTokenState
				case '\'':
					state = signleQuoteState
				case '"':
					state = doubleQuoteState
				default:
					if !unicode.IsSpace(c) {
						currArg.WriteRune(c)
						state = normalTokenState
					} else if state == normalTokenState {
						// Whitespace ends the token; start a new one
						argList = append(argList, currArg.String())
						currArg.Reset()
						state = noTokenState
					}
				}
			default:
				return nil, errors.New(fmt.Sprintf("illegal argument: %s", arguments))
			}
		}
	}

	// If we're still escaped, put in the backslash
	if escaped {
		currArg.WriteRune('\\')
		argList = append(argList, currArg.String())
	} else if state != noTokenState { // Close the last argument if we haven't yet
		argList = append(argList, currArg.String())
	}

	return argList, nil
}
