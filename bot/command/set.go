package command

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"reflect"
	"strconv"
	"strings"
)

var set Command

var settingFields = `Available settings:
	filter.type.sexual        int
	filter.type.lo            bool
	filter.type.grotesque     bool
	filter.type.violent       bool
	filter.type.homosexual    bool
	filter.type.drug          bool
	filter.type.thoughts      bool
	filter.type.antisocial    bool
	filter.type.religion      bool
	filter.type.original      bool
	filter.type.furry         bool
	filter.type.bl            bool
	filter.type.yuri          bool
`

func init() {
	set = Command{
		Run: func(args []string, env *Environment) (*discordgo.MessageSend, error) {
			defer func() {
				if r := recover(); r != nil {
					env.Logger.Errorf("panic recovered: %x", r)
				}
			}()

			if len(args) < 1 {
				return &discordgo.MessageSend{
					Content: "at least one argument is needed",
				}, nil
			}

			assig := strings.Split(args[0], "=")
			if len(assig) < 2 {
				return &discordgo.MessageSend{
					Content: "both an expression and a value are needed",
				}, nil
			}

			lhs := assig[0]
			rhs := assig[1]

			exprs := strings.Split(lhs, ".")
			switch exprs[0] {
			case "filter":
				env.Filter.Lck.Lock()
				defer env.Filter.Lck.Unlock()

				cur := reflect.ValueOf(&env.Filter.Filter).Elem()
				for i := 1; i < len(exprs); i++ {
					cur = cur.FieldByName(strings.Title(exprs[i]))
				}

				switch cur.Type().Name() {
				case "bool":
					val, err := strconv.ParseBool(rhs)
					if err != nil {
						return &discordgo.MessageSend{
							Content: err.Error(),
						}, nil
					}
					cur.SetBool(val)
				case "int":
					val, err := strconv.ParseInt(rhs, 10, 64)
					if err != nil {
						return &discordgo.MessageSend{
							Content: err.Error(),
						}, nil
					}
					cur.SetInt(val)
				case "string":
					cur.SetString(rhs)
				default:
					return &discordgo.MessageSend{
						Content: fmt.Sprintf(`cannot set "%s"`, lhs),
					}, nil
				}

			default:
				return &discordgo.MessageSend{
					Content: fmt.Sprintf(`unknown system setting: "%s"`, exprs[0]),
				}, nil
			}

			return &discordgo.MessageSend{
				Content: fmt.Sprintf(`"%s" has been set to "%s"`, lhs, rhs),
			}, nil
		},
		Name:      "set",
		UsageLine: "set <expr>=<value>",
		Short:     "set a system setting",
		Long:      "Set an internal system setting, where the left-hand-side is expression and the right-hand-side is the value. \n\n" + settingFields,
	}
}
