package command

import (
	"bytes"
	"fmt"
	"text/template"

	"github.com/bwmarrin/discordgo"
)

const HelpTmplSrcGlobal = `r18bot selects daily popular R-18 artwork from pixiv.net

Usage:
	{{.Mention}} <Command> [arguments]

The commands are:
	{{range .Commands}}{{.Name}}		{{.Short}}
	{{end}}

Use "{{.Mention}} help <command>" for more information about a command.
`

const HelpTmplSrcCommand = `Help manual for {{.Command.Name}} command

Usage:
	{{.Mention}} {{.Command.UsageLine}}

{{if .Command.Aliases -}}
Command aliases:
	{{range $i, $v := .Command.Aliases}}{{if $i}}, {{end}}{{$v}}{{end}}

{{end -}}
{{.Command.Long}}
`

var (
	globalHelpTmpl  = template.Must(template.New("help").Parse(HelpTmplSrcGlobal))
	commandHelpTmpl = template.Must(template.New("help").Parse(HelpTmplSrcCommand))

	help Command
)

func init() {
	help = Command{
		Run: func(args []string, env *Environment) (*discordgo.MessageSend, error) {
			var topic *Command
			if len(args) != 0 {
				var err error
				topic, err = byName(args[0])
				if err != nil {
					return &discordgo.MessageSend{
						Content: fmt.Sprintf(`unknown help topic %s. Run "%s help".`, args[0], env.Me.Mention()),
					}, nil
				}
			}

			var out bytes.Buffer
			if topic == nil {
				data := struct {
					Mention  string
					Commands []*Command
				}{
					env.Me.Mention(),
					commands,
				}
				globalHelpTmpl.Execute(&out, data)
			} else {
				data := struct {
					Mention string
					Command *Command
				}{
					env.Me.Mention(),
					topic,
				}
				commandHelpTmpl.Execute(&out, data)
			}

			return &discordgo.MessageSend{
				Content: out.String(),
			}, nil
		},
		Name:      "help",
		UsageLine: "help <command>",
		Short:     "display help manual",
		Long:      "Help displays the help manual for given command, or a available command list if no command specified.",
	}
}
